import { Outlet } from "react-router-dom";

const Posts = () => {
  return (
    <>
      <div>Posts pages</div>
      <Outlet />
    </>
  );
};

export default Posts;
