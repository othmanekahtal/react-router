import { useParams } from "react-router-dom";

const Post = () => {
  const params = useParams();
  return (
    <>
      <h2>{params.id}</h2>
    </>
  );
};

export default Post;
