import { Navigate, Route, Routes } from "react-router-dom";
import Home from "./../pages/home";
import Posts from "./../pages/posts";
import Post from "./../pages/post";

const Views = () => (
  <Routes>
    <Route index element={<Home />} />
    <Route path="/home" element={<Home />} />
    {/* redirect all routes that start with the /dashboard*/}
    <Route path="/dashboard/*" element={<Navigate to="/home" />} />
    <Route path="/posts" element={<Posts />}>
      <Route index element={<div>This is index</div>} />
      <Route path=":id" element={<Post />} />
    </Route>
    <Route path="*" element={<div>Not found</div>} />
  </Routes>
);

export default Views;
